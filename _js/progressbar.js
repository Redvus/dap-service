;
(function ($) {

    // var Scrollbar = window.Scrollbar;
    // Scrollbar.init(document.getElementById('main-scrollbar'), {
    //     speed: 1,
    //     overscrollEffectColor: ''
    // });

    function scrollbarAllSmooth() {
        $('body').niceScroll({
            cursorcolor: "#ffffff",
            cursorwidth: "5px",
            cursorborder: "1px solid #fff",
            scrollspeed: 40,
            mousescrollstep: 12
        });
    }

    if (document.body.clientWidth > 420 || screen.width > 420) {
        scrollbarAllSmooth();
    }

    // var $window = document.window;
    // var scrollTime = 1.2;
    // var scrollDistance = 170;

    // $window.on("mousewheel DOMMouseScroll", function (event) {

    //     event.preventDefault();

    //     var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
    //     var scrollTop = $window.scrollTop();
    //     var finalScroll = scrollTop - parseInt(delta * scrollDistance);

    //     TweenMax.to($window, scrollTime, {
    //         scrollTo: {
    //             y: finalScroll,
    //             autoKill: true
    //         },
    //         ease: Power1.easeOut,
    //         overwrite: 5
    //     });

    // });

}(jQuery));