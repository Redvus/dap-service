;(function ($) {

    var sectionTop = document.getElementById('sectionTop'),
        sectionSecond = document.getElementById('sectionSecond'),
        sectionCompany = document.getElementById('sectionCompany'),
        sectionService = document.getElementById('sectionService'),
        sectionPartners = document.getElementById('sectionPartners'),
        sectionCost = document.getElementById('sectionCost'),
        sectionLogo = document.getElementById('sectionLogoTop'),
        sectionTitleTop = document.getElementById('sectionTitleTop'),
        sectionTitlePlane = document.getElementById('sectionTitlePlane'),
        sectionSubtitlePlane = document.getElementById('sectionSubtitlePlane'),
        sectionPlane = document.getElementById('sectionPlane'),
        sectionHelicopter = document.getElementById('sectionHelicopter'),
        sectionPlaneLeft = document.querySelector('.section-image__block--top'),
        sectionHelicopterRight = document.querySelector('.section-image__block--bottom'),
        sectionTitleCompany = document.getElementById('sectionTitleCompany'),
        sectionTextCompany = document.getElementById('sectionTextCompany'),
        sectionTitleService = document.getElementById('sectionTitleService'),
        sectionTextService = document.getElementById('sectionTextService'),
        sectionTitlePartners = document.getElementById('sectionTitlePartners'),
        RoyalFlightLogo = document.getElementById('RoyalFlightLogo'),
        AKRussiaLogo = document.getElementById('AKRussiaLogo'),
        TravelDubaiLogo = document.getElementById('TravelDubaiLogo'),
        OnurairLogo = document.getElementById('OnurairLogo'),
        EllinairLogo = document.getElementById('EllinairLogo'),
        NordStarLogo = document.getElementById('NordStarLogo'),
        contactForm = document.getElementById('contactForm'),
        ajaxForm = document.querySelector('.ajax-form'),
        contactFormButton = document.getElementById('contactFormButton'),
        contactFormClose = document.getElementById('contactFormClose'),
        wrapperSection = document.querySelector('.wrapper'),
        wrapperBackground = document.querySelector('.wrapper-background'),
        contactSectionPhone = document.getElementById('contactSectionPhone'),
        header = document.querySelector('.header'),
        contactHeaderButton = document.getElementById('contactHeaderButton'),
        contactHeaderMailMobile = document.getElementById('contactHeaderMailMobile'),
        sectionTitleCost = document.getElementById('sectionTitleCost'),
        sectionTextCost = document.getElementById('sectionTextCost'),
        arrowDown = document.querySelector('.arrow-down')
    ;

    var controller = new ScrollMagic.Controller({
        // refreshInterval: 300
    });

    /*----------  Top Section  ----------*/
    function sectionTopAction() {
        var tl = new TimelineMax();
        tl
            .fromTo([contactFormButton, contactSectionPhone], 0.6, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=1.6")
            .fromTo(sectionTitleTop, 0.7, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.8,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.7")
            .fromTo(sectionLogo, 1, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.4")
            .fromTo(sectionTitlePlane, 0.5, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(sectionSubtitlePlane, 0.5, {
                scale: 0.6,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(sectionPlane, 0.8, {
                scale: 0.2,
                xPercent: 0,
                yPercent: 20,
                rotation: 0,
                autoAlpha: 0
            }, {
                scale: 1,
                yPercent: 0,
                rotation: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
            .fromTo(sectionHelicopter, 0.8, {
                scale: 0.2,
                xPercent: 0,
                rotation: 0,
                yPercent: 50,
                autoAlpha: 0
            }, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(header, 1, {
                yPercent: "-100%",
                zIndex: -1,
                autoAlpha: 0
            }, {
                yPercent: 0,
                zIndex: 9990,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionTopScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionSecond,
            triggerHook: 1,
            duration: '100%'
        })
            // .setClassToggle(header, 'is-visible')
            .setTween(tl)
            // .addIndicators({
            //     name: 'topSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Company Section  ----------*/
    function sectionCompanyAction() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionSubtitlePlane, 0.3, {
                scale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            })
            .fromTo(sectionTitlePlane, 0.3, {
                scale: 1
            }, {
                scale: 0.8,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionPlane, 0.6, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0
            }, {
                xPercent: "-60%",
                yPercent: "70%",
                rotation: "-50deg",
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionHelicopter, 0.6, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0
            }, {
                xPercent: "60%",
                yPercent: "-50%",
                rotation: "50deg",
                ease: Power1.easeInOut
            }, "-=0.5")
            .fromTo(sectionTitleCompany, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCompany, 0.6, {
                yPercent: 30,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
        ;

        var sectionCompanyScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionCompany,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'companySection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Service Section  ----------*/
    function sectionServiceAction() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionTitleCompany, 0.6, {
                sacale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCompany, 0.6, {
                yPercent: 0
            }, {
                yPercent: 30,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitleService, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionTextService, 0.6, {
                yPercent: 30,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
            .fromTo(sectionPlane, 0.6, {
                xPercent: "-60%",
                yPercent: "70%",
                rotation: "-50deg"
            }, {
                xPercent: "-65%",
                yPercent: "73%",
                rotation: "-55deg",
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionHelicopter, 0.6, {
                xPercent: "60%",
                yPercent: "-50%",
                rotation: "50deg"
            }, {
                xPercent: "65%",
                yPercent: "-40%",
                rotation: "55deg",
                ease: Power1.easeInOut
            }, "-=0.8")
        ;

        var sectionServiceScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionService,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'ServiceSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Cost Section  ----------*/
    function sectionCostAction() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionTitleService, 0.6, {
                sacale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextService, 0.6, {
                yPercent: 0
            }, {
                yPercent: 30,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitleCost, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionTextCost, 0.6, {
                yPercent: 30,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
        ;

        var sectionCostScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionCost,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'FormSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Partners Section  ----------*/
    function sectionPartnersAction() {
        var tl = new TimelineMax();
        tl
            .fromTo(arrowDown, 0.3, {
                autoAlpha: 1
            }, {
                autoAlpha: 0
            }, "-=0.3")
            .fromTo(sectionPlane, 0.6, {
                xPercent: "-65%",
                yPercent: "73%",
                rotation: "-55deg"
            }, {
                xPercent: "-100%",
                yPercent: "110%",
                rotation: "-65deg",
                scale: 1.3,
                autoAlpha: 0,
                display: "none",
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionHelicopter, 0.6, {
                xPercent: "65%",
                yPercent: "-40%",
                rotation: "55deg"
            }, {
                xPercent: "100%",
                yPercent: "0",
                rotation: "65deg",
                scale: 1.3,
                autoAlpha: 0,
                display: "none",
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitleCost, 0.6, {
                scale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCost, 0.6, {
                yPercent: 0
            }, {
                yPercent: 30,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitlePartners, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(RoyalFlightLogo, 0.6, {
                xPercent: 0,
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(AKRussiaLogo, 0.6, {
                xPercent: "-15%",
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(TravelDubaiLogo, 0.6, {
                xPercent: "15%",
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(OnurairLogo, 0.6, {
                xPercent: "-15%",
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(EllinairLogo, 0.6, {
                xPercent: 0,
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(NordStarLogo, 0.6, {
                xPercent: "15%",
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
        ;

        var sectionPartnersScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionPartners,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'PartnersSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Form Section  ----------*/
    function visibleFormModal() {

        var tl = new TimelineMax({
            paused: true,
            reversed: true
        });

        tl
            .fromTo(contactForm, 0.3, {
                autoAlpha: 0,
                zIndex: -1
            }, {
                autoAlpha: 1,
                zIndex: 9999,
                ease: Power1.easeInOut
            }, "-=0.3")
            .fromTo(ajaxForm, 0.8, {
                scale: 0.95,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(contactFormClose, 0.8, {
                scale: 0.9,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "+=0.2")
        ;

        contactFormButton.onclick = function () {
            tl.restart();
        };

        contactHeaderButton.onclick = function () {
            tl.restart();
        };

        contactFormClose.onclick = function () {
            tl.reverse(-0.4);
        };
    }

    /*----------  Scroll to Section  ----------*/
    function sectionAnchorScroll() {
        var controllerAnchor = new ScrollMagic.Controller({
            globalSceneOptions: {
                duration: '100%',
                triggerHook: 0.025,
                reverse: true
            }
        });

        /*
        object to hold href values of links inside our nav with
        the class '.anchor-nav'

        scene_object = {
        '[scene-name]' : {
            '[target-scene-id]' : '[anchor-href-value]'
        }
        }
        */
        var scenes = {
            'intro': {
                'sectionIntro': 'anchorIntro'
            },
            'scene2': {
                'sectionSecond': 'anchorSecond'
            },
            'scene3': {
                'sectionCompany': 'anchorCompany'
            },
            'scene4': {
                'sectionService': 'anchorService'
            },
            'scene5': {
                'sectionCost': 'anchorCost'
            },
            'scene6': {
                'sectionPartners': 'anchorPartners'
            }
        }

        for (var key in scenes) {
            // skip loop if the property is from prototype
            if (!scenes.hasOwnProperty(key)) continue;

            var obj = scenes[key];

            for (var prop in obj) {
                // skip loop if the property is from prototype
                if (!obj.hasOwnProperty(prop)) continue;

                new ScrollMagic.Scene({
                        triggerElement: '#' + prop
                    })
                    .setClassToggle('#' + obj[prop], 'active')
                    .addTo(controllerAnchor);
            }
        }

        // Change behaviour of controller
        // to animate scroll instead of jump
        controllerAnchor.scrollTo(function (target) {

            TweenMax.to(window, 2, {
                scrollTo: {
                    y: target,
                    autoKill: true // Allow scroll position to change outside itself
                },
                ease: Power2.easeInOut
            });

        });

        //  Bind scroll to anchor links using Vanilla JavaScript
        var anchor_nav = document.querySelector('.nav');

        anchor_nav.addEventListener('click', function (e) {
            var target = e.target,
                id = target.getAttribute('href');

            if (id !== null) {
                if (id.length > 0) {
                    e.preventDefault();
                    controllerAnchor.scrollTo(id);

                    if (window.history && window.history.pushState) {
                        history.pushState("", document.title, id);
                    }
                }
            }
        });
    }

    /*----------  Back Move  ----------*/
    var cloudOne = document.getElementById('cloudOne'),
        cloudTwo = document.getElementById('cloudTwo'),
        cloudThree = document.getElementById('cloudThree'),
        cloudFour = document.getElementById('cloudFour'),
        cloudFive = document.getElementById('cloudFive'),
        cloudSix = document.getElementById('cloudSix')
    ;

    function backgroundCloudMoveOne() {

        var tl = new TimelineMax({
            repeat: -1
        });

        tl
            .fromTo(cloudOne, 25, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: 200,
                yPercent: 50,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=25")
            .fromTo(cloudThree, 22, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: -200,
                yPercent: 50,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=20")
            .fromTo(cloudFive, 20, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: 0,
                yPercent: -170,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=15")
        ;

        return tl;
    }

    function backgroundCloudMoveSecond() {

        var tl = new TimelineMax({
            repeat: -1
        });

        tl
            .fromTo(cloudTwo, 23, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: 0,
                yPercent: 170,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=13")
            .fromTo(cloudFour, 25, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: 200,
                yPercent: -50,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=8")
            .fromTo(cloudSix, 24, {
                autoAlpha: 1,
                scale: 1,
                ease: Power1.easeInOut
            }, {
                scale: 0,
                xPercent: -200,
                yPercent: -50,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=3")
        ;

        return tl;
    }

    /*=======================================
    =            Function Mobile            =
    =======================================*/

    /*----------  Top Section Mobile  ----------*/
    function sectionTopActionMobile() {
        var tl = new TimelineMax();
        tl
            .fromTo([contactFormButton, contactSectionPhone], 0.6, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=1.6")
            .fromTo(sectionTitleTop, 0.7, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.8,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.7")
            .fromTo(sectionLogo, 1, {
                scale: 1,
                autoAlpha: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.4")
            .fromTo(sectionTitlePlane, 0.5, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(sectionSubtitlePlane, 0.5, {
                scale: 0.6,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(sectionPlane, 0.8, {
                scale: 0.2,
                xPercent: 0,
                yPercent: 20,
                rotation: 0,
                autoAlpha: 0
            }, {
                scale: 1,
                yPercent: 0,
                rotation: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
            .fromTo(sectionHelicopter, 0.8, {
                scale: 0.2,
                xPercent: 0,
                rotation: 0,
                yPercent: 50,
                autoAlpha: 0
            }, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.8")
        ;

        var sectionTopScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionSecond,
            triggerHook: 1,
            duration: '100%'
        })
            // .setClassToggle(header, 'is-visible')
            .setTween(tl)
            // .addIndicators({
            //     name: 'topSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Company Section Mobile  ----------*/
    function sectionCompanyActionMobile() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionSubtitlePlane, 0.3, {
                scale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            })
            .fromTo(sectionTitlePlane, 0.3, {
                scale: 1
            }, {
                scale: 0.8,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionPlane, 0.6, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0
            }, {
                xPercent: "-100%",
                yPercent: "70%",
                rotation: "-50deg",
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionHelicopter, 0.6, {
                scale: 1,
                xPercent: 0,
                yPercent: 0,
                rotation: 0
            }, {
                xPercent: "100%",
                yPercent: "-50%",
                rotation: "50deg",
                ease: Power1.easeInOut
            }, "-=0.5")
            .fromTo(sectionTitleCompany, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCompany, 0.6, {
                yPercent: 30,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
        ;

        var sectionCompanyScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionCompany,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'companySection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Service Section  ----------*/
    function sectionServiceActionMobile() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionTitleCompany, 0.6, {
                sacale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCompany, 0.6, {
                yPercent: 0
            }, {
                yPercent: 30,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitleService, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(sectionTextService, 0.6, {
                yPercent: 30,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.4")
        ;

        var sectionServiceScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionService,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'ServiceSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Partners Section  ----------*/
    function sectionPartnersActionMobile() {
        var tl = new TimelineMax();
        tl
            .fromTo(sectionTitleCost, 0.6, {
                scale: 1
            }, {
                scale: 0.5,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .fromTo(sectionTextCost, 0.6, {
                yPercent: 0
            }, {
                yPercent: 30,
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.8")
            .fromTo(sectionTitlePartners, 0.6, {
                scale: 1.2,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(RoyalFlightLogo, 0.6, {
                xPercent: 0,
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(AKRussiaLogo, 0.6, {
                xPercent: "-15%",
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(TravelDubaiLogo, 0.6, {
                xPercent: "15%",
                yPercent: "-25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(OnurairLogo, 0.6, {
                xPercent: "-15%",
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(EllinairLogo, 0.6, {
                xPercent: 0,
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
            .fromTo(NordStarLogo, 0.6, {
                xPercent: "15%",
                yPercent: "25%",
                scale: 1.2,
                autoAlpha: 0
            }, {
                xPercent: 0,
                yPercent: 0,
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "-=0.2")
        ;

        var sectionPartnersScrollAction = new ScrollMagic.Scene({
            triggerElement: sectionPartners,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(tl)
            // .addIndicators({
            //     name: 'PartnersSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Form Modal Mobile  ----------*/
    function visibleFormModalMobile() {

        var tl = new TimelineMax({
            paused: true,
            reversed: true
        });

        tl
            .fromTo(contactForm, 0.3, {
                autoAlpha: 0,
                zIndex: -1
            }, {
                autoAlpha: 1,
                zIndex: 9999,
                ease: Power1.easeInOut
            }, "-=0.3")
            .fromTo(ajaxForm, 0.8, {
                scale: 0.95,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
            .fromTo(contactFormClose, 0.8, {
                scale: 0.9,
                autoAlpha: 0
            }, {
                scale: 1,
                autoAlpha: 1,
                ease: Power1.easeInOut
            }, "+=0.2")
        ;

        contactHeaderMailMobile.onclick = function () {
            tl.restart();
        };

        contactFormClose.onclick = function () {
            tl.reverse(-0.4);
        };
    }

    /*=====  End of Function Mobile  ======*/


    function initDesktop() {
        sectionTopAction();
        sectionCompanyAction();
        sectionServiceAction();
        sectionPartnersAction();
        sectionCostAction();
        visibleFormModal();
        sectionAnchorScroll();
        backgroundCloudMoveOne();
        backgroundCloudMoveSecond();
    }

    function initMobile() {
        sectionTopActionMobile();
        sectionCompanyActionMobile();
        sectionServiceActionMobile();
        sectionPartnersActionMobile();
        sectionCostAction();
        visibleFormModalMobile();
    }

    if (document.body.clientWidth > 420 || screen.width > 420) {

        window.onload = function () {
            initDesktop();
        };

    } else {

        window.onload = function () {
            initMobile();
        };
    }

}(jQuery));